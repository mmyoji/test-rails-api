# frozen_string_literal: true
#
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

(1..3).each do |n|
  email       = "user#{n}@example.com"
  email_login = UserEmailLogin.find_or_create_by!(email: email) do |email_login|
    email_login.user = User.create!
    email_login.password = "password"
  end

  user = email_login.user

  user.create_point! if user.point.nil?

  if user.products.blank?
    user.products.create!(
      title: "user#{n}'s product",
      price: "#{n}000".to_i,
    )
  end
end
