# frozen_string_literal: true

class CreateUserLoginPasswords < ActiveRecord::Migration[7.0]
  def change
    create_table :user_login_passwords do |t|
      t.references :user, null: false, foreign_key: true, index: { unique: true }
      t.string :password_digest, null: false

      t.timestamps
    end
  end
end
