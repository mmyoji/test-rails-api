# frozen_string_literal: true

class CreateUserEmailLogins < ActiveRecord::Migration[7.0]
  def change
    create_table :user_email_logins do |t|
      t.references :user, null: false, foreign_key: true, index: { unique: true }
      t.string :email, null: false, index: { unique: true }
      t.string :password_digest, null: false

      t.timestamps
    end
  end
end
