# frozen_string_literal: true

class DropUserLoginPasswords < ActiveRecord::Migration[7.0]
  def up
    drop_table :user_login_passwords
  end

  def down
    create_table :user_login_passwords do |t|
      t.references :user, null: false, foreign_key: true, index: { unique: true }
      t.string :password_digest, null: false

      t.timestamps
    end
  end
end
