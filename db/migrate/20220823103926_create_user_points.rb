# frozen_string_literal: true

class CreateUserPoints < ActiveRecord::Migration[7.0]
  def change
    create_table :user_points do |t|
      t.references :user, index: { unique: true }, null: false, foreign_key: true
      t.integer :amount, default: 10_000, null: false

      t.timestamps
    end
  end
end
