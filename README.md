# README

This projects runs on:

- Ruby v3.1.2
- Ruby on Rails v7.0.3.1

## Requirements

- Docker
- make

## Development

```sh
# Setup:
#   1. Prepare your own .env file
$ cp .env.example .env
#   2. Prepare master.key file
$ echo <SECRET VALUE> >> config/master.key
$ chmod 0600 config/master.key
#   3. Run setup command
#     - Bulld & pull docker image
#     - Start dev server
#     - Setup databases
$ make setup

# Run test:
$ make test
```
