# frozen_string_literal: true

class Api::UsersController < Api::BaseController
  #
  # POST /api/users
  #
  # Parameters:
  #   user:
  #     email: string
  #     password: string
  #
  def create
    email_login = UserEmailLogin.new(signup_params)
    email_login.signup!

    render json: { data: { access_token: email_login.access_token } }
  end

  private

  def signup_params
    params.require(:user).permit(:email, :password)
  end
end
