# frozen_string_literal: true

class Api::BaseController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound do
    render json: { errors: [I18n.t("views.record_not_found")] }, status: :not_found
  end

  rescue_from ActiveRecord::RecordInvalid do |err|
    render json: { errors: err.record.errors.full_messages }, status: :unprocessable_entity
  end

  rescue_from UserAuthError do
    render json: { errors: [I18n.t("views.not_authenticated")] }, status: :unauthorized
  end

  attr_reader :current_user
  helper_attr :current_user

  private

  def authenticate_user!
    raise UserAuthError unless request.headers.key?("authorization")

    access_token = request.headers["authorization"].split(" ").last
    payload = JsonWebToken.new.verify(access_token)
    raise UserAuthError if payload.nil?

    @current_user = User.find(payload[:id])
  rescue ActiveRecord::RecordNotFound
    raise UserAuthError
  end
end
