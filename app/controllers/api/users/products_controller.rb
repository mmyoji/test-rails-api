# frozen_string_literal: true

class Api::Users::ProductsController < Api::BaseController
  #
  # GET /api/users/:user_id/products
  #
  # Parameters:
  #   per:    (optional) number
  #   before: (optional) string (ISO8601 datetime)
  #
  def index
    load_user

    query = ProductsQuery.new(page_params)
    @products = query.call(@user.products).preload(:order, :user)
    render "api/products/index"
  end

  #
  # GET /api/users/:user_id/products/:id
  #
  def show
    load_user

    @product = @user.products.find(params[:id])
    render "api/products/show"
  end

  private

  def load_user
    @user = User.find(params[:user_id])
  end

  def page_params
    {
      per: params[:per],
      before: params[:before],
    }
  end
end
