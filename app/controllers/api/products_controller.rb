# frozen_string_literal: true

class Api::ProductsController < Api::BaseController
  before_action :authenticate_user!, only: %i(create update destroy)

  #
  # GET /api/products
  #
  # Parameters:
  #   per:    (optional) number
  #   before: (optional) string (ISO8601 datetime)
  #
  def index
    query = ProductsQuery.new(page_params)
    @products = query.call.preload(:order, :user)
  end

  #
  # GET /api/products/:id
  #
  def show
    @product = Product.find(params[:id])
  end

  #
  # POST /api/products
  #
  # Parameters:
  #   product:
  #     title: string
  #     price: number
  #
  def create
    @product = current_user.products.create!(product_params)
    render status: :created
  end

  #
  # PATCH /api/products/:id
  #
  # Parameters:
  #   product:
  #     title: (optional) string
  #     price: (optional) number
  #
  def update
    @product = current_user.products.find(params[:id])
    @product.update!(product_params)
  end

  #
  # DELETE /api/products/:id
  #
  def destroy
    product = current_user.products.find(params[:id])
    product.destroy!
    head :ok
  end

  private

  def page_params
    {
      per: params[:per],
      before: params[:before],
    }
  end

  def product_params
    params.require(:product).permit(:title, :price)
  end
end
