# frozen_string_literal: true

class Api::TokensController < Api::BaseController
  #
  # POST /api/tokens
  #
  # Parameters:
  #   user:
  #     email: string
  #     password: string
  #
  def create
    email_login = UserEmailLogin.find_by(email: token_params[:email])

    if email_login&.authenticate(token_params[:password])
      render json: { data: { access_token: email_login.access_token } }
    else
      render json: { errors: [I18n.t("views.tokens.errors.unauthorized")] }, status: :unauthorized
    end
  end

  private

  def token_params
    params.require(:user).permit(:email, :password)
  end
end
