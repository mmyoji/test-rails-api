# frozen_string_literal: true

class Api::OrdersController < Api::BaseController
  before_action :authenticate_user!, only: %i(create)

  #
  # POST /api/orders
  #
  # Parameters:
  #   order:
  #     product_id: number
  #
  def create
    product = Product.where.not(user_id: current_user.id).find(order_params[:product_id])
    if current_user.request_order(product)
      head :created
    else
      render json: { errors: [I18n.t("views.orders.errors.order_failed")] }, status: :bad_request
    end
  end

  private

  def order_params
    params.require(:order).permit(:product_id)
  end
end
