json.extract! product, :id, :title, :price

json.created_at product.created_at.iso8601
json.updated_at product.updated_at.iso8601

json.sold_out product.order.present?

json.user do
  json.extract! product.user, :id, :email
end
