json.data do
  json.products @products, partial: "api/products/product", as: :product
end
