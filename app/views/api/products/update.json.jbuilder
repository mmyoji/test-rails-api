json.data do
  json.product @product, partial: "api/products/product", as: :product
end
