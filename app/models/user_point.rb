# frozen_string_literal: true

class UserPoint < ApplicationRecord
  INITIAL_AMOUNT = 10_000

  belongs_to :user

  validates :amount, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :user_id, uniqueness: true

  def purchaseable?(price)
    amount >= price
  end
end
