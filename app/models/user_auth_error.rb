# frozen_string_literal: true

class UserAuthError < StandardError
  def initialize(msg = "You're not authenticated")
    super(msg)
  end
end
