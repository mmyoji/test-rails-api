# frozen_string_literal: true

class JsonWebToken
  ALGORITHM = "HS256"
  SECRET = Rails.application.credentials.jwt_secret_key

  def generate(user_payload)
    JWT.encode(
      user_payload.merge(exp: 24.hours.since.to_i),
      SECRET,
      ALGORITHM,
    )
  end

  def verify(token)
    payload, _header = JWT.decode(
      token,
      SECRET,
      true,
      {
        algorithm: ALGORITHM,
      },
    )

    payload.with_indifferent_access
  rescue JWT::DecodeError, JWT::ExpiredSignature
    nil
  end
end
