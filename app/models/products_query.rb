# frozen_string_literal: true

class ProductsQuery
  DEFAULT_PER = 20

  attr_reader :per, :before

  def initialize(params = {})
    @per = parse_per(params[:per])
    @before = parse_before(params[:before])
  end

  def call(products = Product.all)
    if before
      products = products.where("products.updated_at < ?", before)
    end

    products.order(updated_at: :desc).limit(per)
  end

  private

  def parse_per(per_param)
    per = per_param.to_i
    per > 0 ? per : DEFAULT_PER
  end

  def parse_before(before_param)
    Time.zone.parse(before_param)
  rescue TypeError
    nil
  end
end
