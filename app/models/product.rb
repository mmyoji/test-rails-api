# frozen_string_literal: true

class Product < ApplicationRecord
  belongs_to :user
  has_one :order

  validates :title, presence: true, length: { in: 3..50 }
  validates :price, presence: true, numericality: { only_integer: true, in: 100..99_999 }
end
