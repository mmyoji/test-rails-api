# frozen_string_literal: true

class UserEmailLogin < ApplicationRecord
  has_secure_password

  belongs_to :user

  validates :email, presence: true, uniqueness: true, email: true
  validates :user_id, uniqueness: true

  def signup!
    transaction do
      self.user = User.create!
      user.create_point!
      save!
    end
  end

  def access_token
    return if new_record?

    JsonWebToken.new.generate(user.jwt_payload)
  end
end
