# frozen_string_literal: true

class User < ApplicationRecord
  has_one :email_login, class_name: "UserEmailLogin", foreign_key: "user_id"
  has_one :point, class_name: "UserPoint", foreign_key: "user_id"
  has_many :products
  has_many :orders, foreign_key: "buyer_id"

  delegate :email, to: :email_login
  delegate :amount, to: :point, prefix: true

  # NOTE: Do not include sensitve data!
  def jwt_payload
    {
      id: id,
      email: email,
    }
  end

  def request_order(product)
    return if product.user == self
    return if product.order.present?

    transaction do
      point.lock!
      seller_point = product.user.point
      seller_point.lock!

      unless point.purchaseable?(product.price)
        raise ActiveRecord::Rollback
      end

      point.amount -= product.price
      seller_point.amount += product.price

      point.save!
      seller_point.save!
      orders.create!(product: product)
    end
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
