# frozen_string_literal: true

class EmailValidator < ActiveModel::EachValidator
  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  def validate_each(record, attribute, value)
    return if value =~ EMAIL_REGEXP

    record.errors.add attribute, (options[:message] || I18n.t("errors.messages.not_an_email"))
  end
end
