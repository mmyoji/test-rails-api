# frozen_string_literal: true

require "test_helper"

class Api::UsersControllerTest < ActionDispatch::IntegrationTest
  test "POST /api/users returns 200 w/ valid params" do
    post "/api/users", params: { user: { email: "foo@example.com", password: "P@ssw0rd" } }

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_not_nil res_body.dig("data", "access_token")
  end

  test "POST /api/users returns 422 w/ invalid params" do
    post "/api/users", params: { user: { email: "foo@", password: "P@ssw0rd" } }

    assert_response :unprocessable_entity
    assert_equal "application/json", @response.media_type

    assert_not_empty res_body.dig("errors")
  end
end
