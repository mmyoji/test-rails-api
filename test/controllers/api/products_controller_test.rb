# frozen_string_literal: true

require "test_helper"

class Api::ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:bob)
    @product = products(:bob_game)
  end

  test "GET /api/products returns 200" do
    get "/api/products"

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal 2, res_body.dig("data", "products").size
  end

  test "GET /api/products returns 200 w/ `per`" do
    get "/api/products", params: { per: 1 }

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal 1, res_body.dig("data", "products").size
  end

  test "GET /api/products/:id returns 200" do
    get "/api/products/#{@product.id}"

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal @product.id, res_body.dig("data", "product", "id")
  end

  test "GET /api/products/:id returns 404 w/ invalid id" do
    get "/api/products/#{@product.id + 100}"

    assert_response :not_found
    assert_equal "application/json", @response.media_type

    errors = res_body.dig("errors")
    assert_equal 1, errors.size
    assert_includes errors, "Target resource is not found"
  end

  test "POST /api/products returns 201" do
    assert_difference "Product.count", 1 do
      post "/api/products", params: { product: { title: "foo", price: 2000 } }, headers: auth_headers(@user), as: :json
    end

    assert_response :created
    assert_equal "application/json", @response.media_type

    assert res_body.dig("data", "product", "id")
  end

  test "POST /api/products returns 422 w/ invalid params" do
    assert_no_difference "Product.count" do
      post "/api/products", params: { product: { title: "", price: 2000 } }, headers: auth_headers(@user), as: :json
    end

    assert_response :unprocessable_entity
    assert_equal "application/json", @response.media_type

    assert_equal 2, res_body.dig("errors").size
  end

  test "POST /api/products returns 401 w/o valid auth headers" do
    assert_no_difference "Product.count" do
      post "/api/products", params: { product: { title: "foo", price: 2000 } }, as: :json
    end

    assert_response :unauthorized
    errors = res_body.dig("errors")
    assert_equal 1, errors.size
    assert_includes errors, "User must be authenticated"
  end

  test "PATCH /api/products/:id returns 200" do
    assert_no_difference "Product.count" do
      patch "/api/products/#{@product.id}", params: { product: { price: 10_000 } }, headers: auth_headers(@user), as: :json
    end

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal @product.id, res_body.dig("data", "product", "id")
    assert_equal 10_000, res_body.dig("data", "product", "price")
  end

  test "DELETE /api/products/:id returns 200" do
    assert_difference "Product.count", -1 do
      delete "/api/products/#{@product.id}", headers: auth_headers(@user), as: :json
    end

    assert_response :success
    assert_equal "application/json", @response.media_type
  end
end
