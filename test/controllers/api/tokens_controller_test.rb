# frozen_string_literal: true

require "test_helper"

class Api::TokensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:bob)
  end

  test "POST /api/tokens returns 200 w/ valid params" do
    post "/api/tokens", params: { user: { email: @user.email, password: "password" } }

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_not_nil res_body.dig("data", "access_token")
  end

  test "POST /api/tokens returns 401 w/ invalid params" do
    post "/api/tokens", params: { user: { email: @user.email, password: "P@ssw0rd" } }

    assert_response :unauthorized
    assert_equal "application/json", @response.media_type

    assert_includes res_body.dig("errors"), "Email or Password is wrong"
  end
end
