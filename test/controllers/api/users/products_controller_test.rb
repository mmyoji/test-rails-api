# frozen_string_literal: true

require "test_helper"

class Api::Users::ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:bob)
  end

  test "GET /api/users/:user_id/products returns 200" do
    get "/api/users/#{@user.id}/products"

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal 1, res_body.dig("data", "products").size
  end

  test "GET /api/users/:user_id/products returns 404 w/ invalid :user_id" do
    get "/api/users/#{@user.id + 10}/products"

    assert_response :not_found
    assert_equal "application/json", @response.media_type

    assert_not_found
  end

  test "GET /api/users/:user_id/products/:id returns 200" do
    product = @user.products.first
    get "/api/users/#{@user.id}/products/#{product.id}"

    assert_response :success
    assert_equal "application/json", @response.media_type

    assert_equal product.id, res_body.dig("data", "product", "id")
  end

  test "GET /api/users/:user_id/products/:id returns 404 w/ invalid :id" do
    product = @user.products.first
    get "/api/users/#{@user.id}/products/#{product.id + 100}"

    assert_response :not_found
    assert_equal "application/json", @response.media_type

    assert_not_found
  end

  test "GET /api/users/:user_id/products/:id returns 404 w/ invalid :user_id" do
    product = @user.products.first
    get "/api/users/#{@user.id + 10}/products/#{product.id}"

    assert_response :not_found
    assert_equal "application/json", @response.media_type

    assert_not_found
  end

  private

  def assert_not_found
    errors = res_body.dig("errors")
    assert_equal 1, errors.size
    assert_includes errors, "Target resource is not found"
  end
end
