# frozen_string_literal: true

require "test_helper"

class Api::OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:bob)
    @product = products(:alice_cloth)
  end

  test "POST /api/orders returns 201" do
    assert_difference "Order.count", 1 do
      post "/api/orders", params: { order: { product_id: @product.id } }, headers: auth_headers(@user), as: :json
    end

    assert_response :created
  end

  test "POST /api/orders returns 400 w/ invalid :product_id" do
    @user.request_order(@product)

    assert_no_difference "Order.count" do
      post "/api/orders", params: { order: { product_id: @product.id } }, headers: auth_headers(@user), as: :json
    end

    assert_response :bad_request

    errors = res_body.dig("errors")
    assert_equal 1, errors.size
    assert_includes errors, "product is already purchased by someone or you don't have enough point"
  end
end
