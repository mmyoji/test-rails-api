# frozen_string_literal: true

require "test_helper"

class UserPointTest < ActiveSupport::TestCase
  test "should set default value on initialization" do
    point = UserPoint.new
    assert_equal 10_000, point.amount
  end
end
