# frozen_string_literal: true

require "test_helper"

class JsonWebTokenTest < ActiveSupport::TestCase
  setup do
    @jwt = JsonWebToken.new
    @user = users(:bob)
  end

  test "should encode/decode JWT" do
    token = @jwt.generate(@user.jwt_payload)

    assert_instance_of String, token

    payload = @jwt.verify(token)

    assert_equal 3, payload.keys.size
    assert_equal @user.id, payload[:id]
    assert_equal @user.email, payload[:email]
    assert_not_nil payload[:exp]
  end

  test "should return nil when expired" do
    token = @jwt.generate(@user.jwt_payload)

    travel_to 24.hours.since do
      payload = @jwt.verify(token)
      assert_nil payload
    end
  end

  test "should return nil when invalid JWT format" do
    [nil, "", "xxx"].each do |value|
      assert_nil @jwt.verify(value)
    end
  end
end
