# frozen_string_literal: true

require "test_helper"

class ProductsQueryTest < ActiveSupport::TestCase
  setup do
    @bob = users(:bob)
    @alice = users(:alice)
  end

  test "should set valid number as `per`" do
    query = ProductsQuery.new
    assert_equal 20, query.per

    query = ProductsQuery.new(per: 10)
    assert_equal 10, query.per

    query = ProductsQuery.new(per: "5")
    assert_equal 5, query.per

    query = ProductsQuery.new(per: "foo")
    assert_equal 20, query.per
  end

  test "should set valid Time object or nil as `before`" do
    query = ProductsQuery.new
    assert_nil query.before

    query = ProductsQuery.new(before: "invalid")
    assert_nil query.before

    query = ProductsQuery.new(before: nil)
    assert_nil query.before

    current = Time.current
    query = ProductsQuery.new(before: current.iso8601)
    assert_instance_of ActiveSupport::TimeWithZone, query.before
    assert_equal ignore_usec(current), ignore_usec(query.before)
  end

  test "should return ordered Product list" do
    create_products(@bob)
    create_products(@alice)

    query = ProductsQuery.new
    products = query.call

    products.each do |product|
      assert_instance_of Product, product
    end

    assert_equal 6, products.size

    all_products = Product.all.order(updated_at: :desc)
    assert_equal products[0].id, all_products[0].id
    assert_equal products[1].id, all_products[1].id
    assert_equal products[2].id, all_products[2].id
    assert_equal products[3].id, all_products[3].id
  end

  test "should return limited Product list w/ `per`" do
    create_products(@bob)
    create_products(@alice)

    query = ProductsQuery.new(per: 3)
    products = query.call

    assert_equal 3, products.size

    all_products = Product.order(updated_at: :desc)
    assert_equal products[0].id, all_products[0].id
    assert_equal products[1].id, all_products[1].id
    assert_equal products[2].id, all_products[2].id
  end

  test "should return filtered Product list w/ `before`" do
    current = Time.current + 1.second
    sleep 1
    create_products(@bob)
    create_products(@alice)

    query = ProductsQuery.new(before: current.iso8601)
    products = query.call

    assert_equal 2, products.size

    all_products = Product.where("products.updated_at < ?", current).order(updated_at: :desc)
    assert_equal products[0].id, all_products[0].id
    assert_equal products[1].id, all_products[1].id
  end

  private

  def create_products(user)
    user.products.create!(
      title: "product-2",
      price: 100,
    )
    user.products.create!(
      title: "product-3",
      price: 200,
    )
  end

  def ignore_usec(time)
    time.change(usec: 0)
  end
end
