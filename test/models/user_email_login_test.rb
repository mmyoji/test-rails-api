# frozen_string_literal: true

require "test_helper"

VALID_EMAIL = "foo@example.com"
VALID_PASSWORD = "P@ssw0rd"

class UserEmailLoginTest < ActiveSupport::TestCase
  test "should have valid format email" do
    email_login = init_for_validation(
      email: "bob@",
    )

    refute email_login.valid?
    assert_equal 1, email_login.errors.size

    email_errors = email_login.errors.where(:email)
    assert_equal 1, email_errors.size

    email_error = email_errors.last
    assert_equal "is not an email", email_error.type
    assert_equal "is not an email", email_error.message
  end

  test "#access_token should return string" do
    email_login = user_email_logins(:bob)

    assert_instance_of String, email_login.access_token
  end

  test "#signup should create a user, their user_point and itself w/ valid email and password" do
    email_login = init_for_signup

    assert email_login.signup!
    assert email_login.persisted?
    assert email_login.user.persisted?
    assert email_login.user.point.persisted?
    assert 10_000, email_login.user.point.amount
    assert_not_nil email_login.access_token
    assert_equal VALID_EMAIL, email_login.email
  end

  test "#signup should not create neither associations nor itself w/ invalid email" do
    email_login = init_for_signup(
      email: "foo@",
    )

    refute_signup(email_login)
    assert_equal 1, email_login.errors.size
    assert_includes email_login.errors.full_messages, "Email is not an email"
  end

  test "#signup should not create neither associations nor itself w/ taken email" do
    email_login = init_for_signup(
      email: user_email_logins(:bob).email,
    )

    refute_signup(email_login)
    assert_equal 1, email_login.errors.size
    assert_includes email_login.errors.full_messages, "Email has already been taken"
  end

  test "#signup should not create neither associations nor itself w/ invalid password" do
    email_login = init_for_signup(
      password: "",
    )

    refute_signup(email_login)
    assert_equal 1, email_login.errors.size
    assert_includes email_login.errors.full_messages, "Password can't be blank"
  end

  private

  def init_for_validation(user: User.create!, email: VALID_EMAIL, password: VALID_PASSWORD)
    UserEmailLogin.new(
      user: user,
      email: email,
      password: password,
    )
  end

  def init_for_signup(email: VALID_EMAIL, password: VALID_PASSWORD)
    UserEmailLogin.new(
      email: email,
      password: password,
    )
  end

  def refute_signup(email_login)
    assert_raises ActiveRecord::RecordInvalid do
      email_login.signup!
    end
    refute email_login.persisted?
    refute email_login.user.persisted?
    refute email_login.user.point.persisted?
    assert_nil email_login.access_token
  end
end
