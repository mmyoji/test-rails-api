# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:bob)
  end

  test "#request_order should success w/ valid data" do
    product = products(:alice_cloth)

    assert_difference "Order.count", 1 do
      @user.request_order(product)
    end

    assert_equal 9_500, @user.point_amount
    assert_equal 10_500, users(:alice).point_amount
  end

  test "#request_order should fail when target product is user's own one" do
    product = products(:bob_game)

    assert_no_difference "Order.count" do
      @user.request_order(product)
    end

    assert_equal 10_000, @user.point_amount
    assert_equal 10_000, users(:alice).point_amount
  end

  test "#request_order should fail when order has already been created" do
    product = products(:alice_cloth)

    assert_difference "Order.count", 1 do
      @user.request_order(product)
    end

    assert_no_difference "Order.count" do
      @user.request_order(product)
    end
  end

  test "#request_order should fail when user doesn't have enough amount of point" do
    product = products(:alice_cloth)
    product.update!(price: 10001)

    assert_no_difference "Order.count" do
      @user.request_order(product)
    end

    assert_equal 10_000, @user.point_amount
    assert_equal 10_000, users(:alice).point_amount
  end
end
