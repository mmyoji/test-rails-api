app_name = api

.PHONY: setup
setup:
	docker compose up -d
	docker compose exec $(app_name) bin/rails db:setup
	docker compose exec -e RAILS_ENV=test $(app_name) bin/rails db:migrate

.PHONY: console
console:
	docker compose exec $(app_name) bin/rails console

.PHONY: test
test:
	docker compose exec $(app_name) bin/rails test --pride
