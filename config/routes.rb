Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  namespace :api do
    defaults format: :json do
      resources :orders, only: %i(create)
      resources :products, only: %i(index show create update destroy)
      resources :users, only: %i(create) do
        scope module: "users" do
          resources :products, only: %i(index show)
        end
      end
      resources :tokens, only: %i(create)
    end
  end
end
